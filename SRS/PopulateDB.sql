DROP DATABASE IF EXISTS myars;
CREATE DATABASE myars;
GRANT ALL PRIVILEGES ON myars.* TO root@localhost;
FLUSH PRIVILEGES;
USE myars;


#1 User Table

CREATE TABLE user (
user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(40) NOT NULL,
last_name VARCHAR(40) NOT NULL,
email VARCHAR(40) UNIQUE NOT NULL,
password VARCHAR(600) NOT NULL,
role VARCHAR(20) DEFAULT "user"
);

#Data

#pass akshay
INSERT INTO user (first_name, last_name, email, password) 
VALUES ('Akshay', 'Kirange', 'kuldeepkirange@gmail.com', '$2a$12$sv.ojho0wlOVvFGD9VI8hOjl4uLK78118jxjrDlj0fX7ZJw0s6feG');

#pass ankit
INSERT INTO user (first_name, last_name, email, password) 
VALUES ('Ankit', 'Dhawale', 'ankitdhawale84@gmail.com', '$2a$12$P1l0N0/DvxOYFYopvmxWz.2LmuaT/W.1p1eQ16m9u9UE2aOU.789K');

#pass mayur
INSERT INTO user (first_name, last_name, email, password) 
VALUES ('Mayur', 'Bhosale', 'bhosale.mayur92@gmail.com', '$2a$12$v.FKdJjMdW612Oz.A4Jf8OWZGj4bzHzoguUFs89ftMcN9BncXF0Ce');

#pass sachin
INSERT INTO user (first_name, last_name, email, password) 
VALUES ('Sachin', 'Bhoi', 'bsach@gmail.com', '$2a$12$NwMprt.xnkuP.77xcAR4UOXKmEobVrkxSseOcPczkPeMSFic59qWi');

# pass admin
INSERT INTO user (first_name, last_name, email, password, role) 
VALUES ('Admin', 'User', 'admin@gmail.com', '$2a$12$Ffqlgk//WWoFJYYyLxoAceT7ib7eqgCUdcflbH4QlQBh3/oPoGGje', 'admin');



#2 Passenger table

CREATE TABLE passenger (
passenger_id INTEGER PRIMARY KEY AUTO_INCREMENT,
title VARCHAR(20) ,
first_name VARCHAR(40) ,
last_name VARCHAR(40) ,
mobile_no BIGINT ,
email VARCHAR(40), 
country VARCHAR(40) ,
pass_state VARCHAR(40) ,
city VARCHAR(40) ,
address VARCHAR(100),
date_of_birth DATE ,
covid_vaccinated INTEGER(1), 
UNIQUE (first_name, last_name, email)
);

#Data
#1
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Akshay', 'Kirange', 9881223465, 'kuldeepkirange@gmail.com', 'India', 'Maharashtra', 'Jalgaon', 'Kolhe Nagar', '1996-06-06', 1);

#2
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Ankit', 'Dhawale', 7803808892, 'ankitdhawale84@gmail.com', 'India', 'Maharashtra', 'Nagpur', 'Sita Bardi', '1995-05-04', 1);

#3
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Mayur', 'Bhosale', 8903886544, 'bhosale.mayur92@gmail.com', 'India', 'Maharashtra', 'Pune', 'Baramati', '1991-04-17', 1);

#4
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Sachin', 'Bhoi', 7765967092, 'bsach@gmail.com', 'India', 'Maharashtra', 'Jalgaon', 'Jamner', '1994-08-14', 1);

#5
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Aakash', 'Khanke', 7765743092, 'aakash@gmail.com', 'India', 'Maharashtra', 'Nagpur', 'Kalmeshwar', '1990-03-24', 1);

#6
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Prashant', 'Patil', 7765965475, 'prashant@gmail.com', 'India', 'Maharashtra', 'Mumbai', 'Airoli', '1995-06-17', 1);

#7
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Abhishek', 'Bodhane', 9874561204, 'abhishek@gmail.com', 'India', 'Rajasthan', 'Jaipur', 'Pink City', '1989-05-21', 1);

#8
INSERT INTO passenger (title, first_name, last_name, mobile_no, email, country, pass_state, city, address, date_of_birth, covid_vaccinated) 
VALUES ('Mr.', 'Saurabh', 'Tiwari', 9781297855, 'saurabh@gmail.com', 'India', 'Madhya Pradesh', 'Bhopal', 'MP Nagar', '1987-02-14', 1);




#3 Flight table

CREATE TABLE flight (
flight_id INTEGER PRIMARY KEY AUTO_INCREMENT,
airline_name VARCHAR(40),
departure_airport_name VARCHAR(40),
arrival_airport_name VARCHAR(40),
source_city VARCHAR(40),
destination_city VARCHAR(40),
business_class_fare DOUBLE ,
economy_class_fare DOUBLE,
business_class_capacity INTEGER,
economy_class_capacity INTEGER,
flight_total_capacity INTEGER, 
UNIQUE (airline_name, departure_airport_name, arrival_airport_name)
);

#4 Schedule table

CREATE TABLE schedule (
schedule_id INTEGER PRIMARY KEY AUTO_INCREMENT,
flight_id INTEGER,
departure_date DATE,
departure_time TIME,
arrival_date DATE,
arrival_time TIME,
flight_status VARCHAR(40) DEFAULT 'Scheduled',
UNIQUE (flight_id, departure_date, departure_time, arrival_date, arrival_time)
); 

#Data
#1 HYD -> BLR
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Vistara','HYD', 'BLR','Hyderabad','Bangalore',12400,4500,50,150,200);
#1a (1)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(1,'2022-04-22','06:30:00','2022-04-22','07:35:00');
#1b (2)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(1,'2022-04-22','18:30:00','2022-04-22','19:35:00');

#2 HYD -> BLR
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Air Asia','HYD', 'BLR','Hyderabad','Bangalore',9000,4600,50,150,200);
#2a (3)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(2,'2022-04-22','08:30:00','2022-04-22','09:45:00');
#2b (4)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(2,'2022-04-22','16:30:00','2022-04-22','17:45:00');

#3 HYD -> BLR
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Indigo','HYD', 'BLR','Hyderabad','Bangalore',10500,4800,50,150,200);
#3a (5)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(3,'2022-04-22','10:30:00','2022-04-22','11:35:00');
#3b (6)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(3,'2022-04-22','15:30:00','2022-04-22','16:35:00');

#4 HYD -> BLR
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Go First','HYD', 'BLR','Hyderabad','Bangalore',11600,4950,50,150,200);
#4a (7)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(4,'2022-04-22','12:30:00','2022-04-22','13:45:00');
#4b (8)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(4,'2022-04-22','19:35:00','2022-04-22','20:50:00');


#5 NGP -> MUM
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Vistara','NGP', 'MUM','Nagpur','Mumbai',11400,4750,50,150,200);
#5a (9)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(5,'2022-04-23','06:30:00','2022-04-23','07:35:00');
#5b (10)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(5,'2022-04-23','18:30:00','2022-04-23','19:35:00');

#6 NGP -> MUM
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Air Asia','NGP', 'MUM','Nagpur','Mumbai',10300,4800,50,150,200);
#6a (11)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(6,'2022-04-23','08:30:00','2022-04-23','09:45:00');
#6b (12)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(6,'2022-04-23','16:30:00','2022-04-23','17:45:00');

#7 NGP -> MUM
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Indigo','NGP', 'MUM','Nagpur','Mumbai',11500,4850,50,150,200);
#7a (13)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(7,'2022-04-23','10:30:00','2022-04-23','11:35:00');
#7b (14)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(7,'2022-04-23','15:30:00','2022-04-23','16:35:00');

#8 NGP -> MUM
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Go First','NGP', 'MUM','Nagpur','Mumbai',9600,4900,50,150,200);
#8a (15)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(8,'2022-04-23','12:30:00','2022-04-23','13:45:00');
#8b (16)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(8,'2022-04-23','19:35:00','2022-04-23','20:50:00');



#9 MUM -> DEL
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Vistara','MUM', 'DEL','Mumbai','Delhi',22000,6500,50,150,200);
#9a (17)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(9,'2022-04-24','06:30:00','2022-04-24','08:30:00');
#9b (18)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(9,'2022-04-24','18:30:00','2022-04-24','20:30:00');

#10 MUM -> DEL
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Air Asia','MUM', 'DEL','Mumbai','Delhi',21750,6800,50,150,200);
#10a (19)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(10,'2022-04-24','08:30:00','2022-04-24','10:30:00');
#10b (20)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(10,'2022-04-24','16:30:00','2022-04-24','18:30:00');

#11 MUM -> DEL
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Indigo','MUM', 'DEL','Mumbai','Delhi',20500,7500,50,150,200);
#11a (21)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(11,'2022-04-24','10:30:00','2022-04-24','12:30:00');
#11b (22)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(11,'2022-04-24','15:30:00','2022-04-24','17:30:00');

#12 MUM -> DEL
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Go First','MUM', 'DEL','Mumbai','Delhi',22350,7600,50,150,200);
#12a (23)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(12,'2022-04-24','12:30:00','2022-04-24','14:30:00');
#12b (24)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(12,'2022-04-24','19:35:00','2022-04-24','21:35:00');





#13 CHN -> POB
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Vistara','CHN', 'POB','Chennai','Port Blair',20000,6500,50,150,200);
#13a (25)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(13,'2022-04-25','06:30:00','2022-04-25','08:30:00');
#13b (26)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(13,'2022-04-25','18:30:00','2022-04-25','20:30:00');

#14 CHN -> POB
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Air Asia','CHN', 'POB','Chennai','Port Blair',20700,7200,50,150,200);
#14a (27)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(14,'2022-04-25','08:30:00','2022-04-25','10:30:00');
#14b (28)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(14,'2022-04-25','16:30:00','2022-04-25','18:30:00');

#15 CHN -> POB
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Indigo','CHN', 'POB','Chennai','Port Blair',22100,6500,50,150,200);
#15a (29)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(15,'2022-04-25','10:30:00','2022-04-25','12:30:00');
#15b (30)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(15,'2022-04-25','15:30:00','2022-04-25','17:30:00');

#16 CHN -> POB
INSERT INTO flight (airline_name,departure_airport_name,arrival_airport_name,source_city,
destination_city,business_class_fare,economy_class_fare,business_class_capacity,economy_class_capacity,flight_total_capacity) 
VALUES('Go First','CHN', 'POB','Chennai','Port Blair',23000,6600,50,150,200);
#16a (31)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(16,'2022-04-25','12:30:00','2022-04-25','14:30:00');
#16b (32)
INSERT INTO schedule (flight_id,departure_date,departure_time,arrival_date,arrival_time) VALUES(16,'2022-04-25','19:35:00','2022-04-25','21:35:00');



#5 Offer table

CREATE TABLE offer (
offer_id INTEGER PRIMARY KEY AUTO_INCREMENT,
promocode VARCHAR(40),
discount DOUBLE,
min_txn_amount DOUBLE,
applicable_on VARCHAR(200) DEFAULT "Valid on all Banks' Debit and Credit cards"
);

#Data
INSERT INTO offer (promocode, discount, min_txn_amount) 
VALUES ('FLY1000', 1000, 10000);

INSERT INTO offer (promocode, discount, min_txn_amount) 
VALUES ('FLY800', 800, 8000);

INSERT INTO offer (promocode, discount, min_txn_amount) 
VALUES ('FLY700', 700, 7000);

INSERT INTO offer (promocode, discount, min_txn_amount) 
VALUES ('FLY600', 600, 6000);

INSERT INTO offer (promocode, discount, min_txn_amount) 
VALUES ('FLY500', 500, 5000);



#6 Package table

CREATE TABLE package (
package_id INTEGER PRIMARY KEY AUTO_INCREMENT,
package_name VARCHAR(40),
seat_type VARCHAR(40),
food VARCHAR(40),
beverages VARCHAR(40),
baggage VARCHAR(40),
package_fare DOUBLE
);


INSERT INTO package (package_name,seat_type,food,beverages,baggage,package_fare)
VALUES('Regular','economy','Breakfast','Hot/Cold beverages','5',500);

INSERT INTO package (package_name,seat_type,food,beverages,baggage,package_fare)
VALUES('Premium','business','Brunch','Hot/Cold beverages','8',800);




#7 Booking table

CREATE TABLE booking (
booking_id INTEGER PRIMARY KEY AUTO_INCREMENT,
passenger_id INTEGER,
schedule_id INTEGER,
user_id INTEGER,
flight_id INTEGER,
package_id INTEGER,
offer_id INTEGER,
booking_date DATE,
passenger_count INTEGER,
fare_per_passenger DOUBLE,
seat_no INTEGER,
total_fare DOUBLE,
ticket_status VARCHAR(40) DEFAULT 'Booked'
);



#Data 
INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (1, 1, 1, 2, 1, 1, '2022-04-20', 1, 12400, 7, 13400);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (2, 3, 1, 1, 2, 2, '2022-04-20', 1, 4600, 45, 5800);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (3, 2, 2, 2, 1, 1, '2022-04-20', 1, 12400, 17, 13400);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (4, 4, 2, 1, 2, 2, '2022-04-20', 1, 4600, 22, 5200);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (5, 9, 3, 2, 1, 5, '2022-04-21', 1, 11400, 15, 13400);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (6, 11, 3, 1, 1, 6, '2022-04-21', 1, 4800, 37, 5400);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (7, 10, 4, 2, 1, 5, '2022-04-21', 1, 11400, 49, 11900);

INSERT INTO booking (passenger_id, schedule_id, user_id, package_id, offer_id, flight_id, booking_date, passenger_count, fare_per_passenger, seat_no, total_fare) 
VALUES (8, 12, 4, 1, 1, 6, '2022-04-21', 1, 4800, 57, 5100);

#8 Payment card table

CREATE TABLE payment_card_detail (
card_id INTEGER PRIMARY KEY AUTO_INCREMENT,
name_on_card VARCHAR(40),
card_no BIGINT UNIQUE,
valid_till DATE,
cvv INTEGER(3),
user_id INTEGER
);

#Data
INSERT INTO payment_card_detail (user_id, name_on_card, card_no, valid_till, cvv) 
VALUES (1, 'Akshay Kirange', 895632147569, '2024-05-01', 147);

INSERT INTO payment_card_detail (user_id, name_on_card, card_no, valid_till, cvv) 
VALUES (2, 'Ankit Dhawale', 756984123658, '2025-06-10', 258);

INSERT INTO payment_card_detail (user_id, name_on_card, card_no, valid_till, cvv) 
VALUES (3, 'Mayur Bhosale', 689321654895, '2023-09-08', 369);

INSERT INTO payment_card_detail (user_id, name_on_card, card_no, valid_till, cvv) 
VALUES (4, 'Sachin Bhoi', 812365985698, '2026-04-16', 521);



















