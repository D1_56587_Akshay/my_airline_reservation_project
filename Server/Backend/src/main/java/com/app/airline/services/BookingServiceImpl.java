package com.app.airline.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.airline.dao.BookingDetailsProjection;
import com.app.airline.dao.IBookingDao;
import com.app.airline.dao.IBookingDetailsProjection;
import com.app.airline.dao.IFlightDao;
import com.app.airline.dao.IPassenger;
import com.app.airline.dao.IScheduleDao;
import com.app.airline.dao.UserDao;
import com.app.airline.dtos.BookingDto;
import com.app.airline.dtos.RandomNoGenerator;
import com.app.airline.pojos.Booking;

@Transactional
@Service
public class BookingServiceImpl {
	@Autowired
	private IBookingDao bDao;
	
	@Autowired
	private UserDao udao;
	
	@Autowired
	private IPassenger pdao;
	
	@Autowired
	private IFlightDao fdao;
	@Autowired
	private IScheduleDao sdao;
	
	public RandomNoGenerator rnd = new RandomNoGenerator();
	
	

	public Booking findBookingById(int id) {
		Booking b1=bDao.findByBookingId(id);
		return b1;
	}
	
	public List<Booking> findAllBookings() {
		List<Booking> bookingList=bDao.findAll();
		return bookingList;
	}
	
	public List<Object []> findBooking(){
		List<Object []> bookingList=bDao.getBookings();
		return bookingList;
	}
	
	public List<Object []> searchBooking(int bookingId, String firstName){
		List<Object []> bookingList=bDao.getBookings();
		return bookingList;
	}

	public Booking addBooking(BookingDto bookingDto) {
		List<Integer> list=bDao.findSeatNo(bookingDto.getScheduleId());
		System.out.println(list);
		Booking booking=new Booking();
		booking.setBookingDate(bookingDto.getBookingDate());
		booking.setTotalFare(bookingDto.getTotalFare());
		booking.setUser(udao.findById(bookingDto.getUserId()));
		booking.setPassenger(pdao.findById(bookingDto.getPassengerId()));
		booking.setFlight(fdao.findByFlightId(bookingDto.getFlightId()));
		booking.setSchedule(sdao.findByScheduleId(bookingDto.getScheduleId()));
		booking.setSeatNo(RandomNoGenerator.getRandomNonRepeatingIntegers(bDao.findSeatNo(bookingDto.getScheduleId()), 1, 20));
		booking.setTicketStatus("Booked");
		return bDao.save(booking);
	}
	
	public  List<IBookingDetailsProjection> getAllBookingDetails() {
		return bDao.getBookingDetails();
	}
	
//	// Get booking details for tickit generation	
//			public List<BookingDetailsProjection> getAllBookingDetails() {
//				return bookingDao.getBookingDetails();
//				
//			}

	public IBookingDetailsProjection getAllBookingDetailsById(int id) {
		
		return bDao.getBookingDetailsById(id);
	}
	
	public List<Integer> getSeatNo(int scheduleId){
		List<Integer> list=bDao.findSeatNo(scheduleId);
		return list;
	}

	public Object checkIn(int bookingId, String firstName) {
		Booking b= bDao.findByBookingIdAndFirstName(bookingId, firstName);
		System.out.println(b);
		if(b==null) {
			return null;
		}
		else if(b.getTicketStatus().equals("Confirmed")) {
			return "Confirmed";
		}else if(b.getTicketStatus().equals( "Cancelled")){
			return "Cancelled";
		}else {
			b.setTicketStatus("Confirmed");
			return bDao.save(b);
		}
		}
	

	public Booking myTrip(int bookingId, String firstName) {
		// TODO Auto-generated method stub
		return bDao.findByBookingIdAndFirstName(bookingId, firstName);
	}

	public Object cancelTicket(int bookingId) {
		Booking b= bDao.findByBookingId(bookingId);
		System.out.println(b);
		if(b==null)
			return null;
		else if(b.getTicketStatus().equals("Cancelled")) {
			return "Cancelled";
		}else {
			b.setTicketStatus("Cancelled");
			return bDao.save(b);
		}
	}
	// Get booking details by User id for tickit generation	
			public List<BookingDetailsProjection> findMyBooking(int userId) {
				return bDao.findMyBooking(userId);
								
			}
	
//	public int getSeatNo(int scheduleId){
//		//List<Integer> list=bDao.findSeatNo(scheduleId);
//		int seatNo=RandomNoGenerator.getRandomNonRepeatingIntegers(bDao.findSeatNo(scheduleId), 10, 20);
//		return seatNo;
//	}
	
	
}
