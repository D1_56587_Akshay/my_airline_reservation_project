package com.app.airline.pojos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "package")
public class Packages {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "package_id")
	private int packageId;
	@Column(name = "package_name")
	private String packageName;
	@Column(name = "seat_type")
	private String seatType;
	private String food;
	private String beverages;
	private String baggage;
	@Column(name = "package_fare")
	private double packageFare;
	
	@OneToMany(mappedBy = "packages")
	private List<Booking> list;
	public Packages() {
		super();
	}
	public Packages(String packageName, String seatType, String food, String beverages, String baggage,
			double packageFare) {
		super();
		this.packageName = packageName;
		this.seatType = seatType;
		this.food = food;
		this.beverages = beverages;
		this.baggage = baggage;
		this.packageFare = packageFare;
	}
	public Packages(int packageId, String packageName, String seatType, String food, String beverages, String baggage,
			double packageFare) {
		super();
		this.packageId = packageId;
		this.packageName = packageName;
		this.seatType = seatType;
		this.food = food;
		this.beverages = beverages;
		this.baggage = baggage;
		this.packageFare = packageFare;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getSeatType() {
		return seatType;
	}
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}
	public String getFood() {
		return food;
	}
	public void setList(List<Booking> list) {
		this.list = list;
	}
	public void setFood(String food) {
		this.food = food;
	}
	public String getBeverages() {
		return beverages;
	}
	public void setBeverages(String beverages) {
		this.beverages = beverages;
	}
	public String getBaggage() {
		return baggage;
	}
	public void setBaggage(String baggage) {
		this.baggage = baggage;
	}
	public double getPackageFare() {
		return packageFare;
	}
	public void setPackageFare(double packageFare) {
		this.packageFare = packageFare;
	}
	@Override
	public String toString() {
		return "Package [packageId=" + packageId + ", packageName=" + packageName + ", seatType=" + seatType + ", food="
				+ food + ", beverages=" + beverages + ", baggage=" + baggage + ", packageFare=" + packageFare + "]";
	}
	
	

}
