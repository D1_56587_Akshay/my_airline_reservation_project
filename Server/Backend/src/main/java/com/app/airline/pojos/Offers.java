package com.app.airline.pojos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "offer ")
public class Offers {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "offer_id")
	private int id;
	@Column(name = "promocode")
	private String promoCode;
	private double discount;
	@Column(name = "min_txn_amount")
	private double  minTxnAmount;
	@Column(name = "applicable_on")
	private String validOn;
	@OneToMany(mappedBy = "offer")
	private List<Booking> list;
	
	public Offers() {
	}

	public Offers(int id, String promocode, double discount, double min_txn_amount) {
		this.id = id;
		this.promoCode = promocode;
		this.discount = discount;
		this.minTxnAmount = min_txn_amount;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promocode) {
		this.promoCode = promocode;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getMinTxnAmount() {
		return minTxnAmount;
	}

	public void setList(List<Booking> list) {
		this.list = list;
	}

	public void setMinTxnAmount(double min_txn_amount) {
		this.minTxnAmount = min_txn_amount;
	}

	public String getValidOn() {
		return validOn;
	}

	public void setValidOn(String valid_on) {
		this.validOn = valid_on;
	}

	@Override
	public String toString() {
		return "Offers [id=" + id + ", promocode=" + promoCode + ", discount=" + discount + ", min_txn_amount="
				+ minTxnAmount + ", valid_on=" + minTxnAmount + "]";
	}

	
	
}
