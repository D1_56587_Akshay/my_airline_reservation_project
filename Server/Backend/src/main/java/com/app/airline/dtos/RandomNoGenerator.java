package com.app.airline.dtos;


import java.util.List;
import java.util.Random;

public class RandomNoGenerator {

	public static int getRandomNonRepeatingIntegers(List<Integer> numbers, int min, int max) {
		
		Random random = new Random();
		boolean flag=false;
		int randomNumber=0;
		while (!flag) {
			// Get Random numbers between range
			randomNumber = random.nextInt((max - min) + 1) + min;
			// Check for duplicate values
			if (!numbers.contains(randomNumber)) {
				numbers.add(randomNumber);
				flag=true;
			}
				
			}
		return randomNumber;
//		}

		
	}
}
