package com.app.airline.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.airline.pojos.Packages;

public interface IPackagesDao extends JpaRepository<Packages, Integer>{
	
	List<Packages> findAll();
	Packages findBypackageId(int id);
}
