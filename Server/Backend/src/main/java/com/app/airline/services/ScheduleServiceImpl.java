package com.app.airline.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.airline.dao.IFlightDao;
import com.app.airline.dao.IScheduleDao;
import com.app.airline.dtos.ScheduleDto;
import com.app.airline.pojos.Schedule;

@Transactional
@Service
public class ScheduleServiceImpl {

	@Autowired
	private IScheduleDao scheduleDao;
	
	@Autowired
	private IFlightDao flightDao;
	
	
	//Add schedule
	public Schedule addSchedule(Schedule schedule) 
	{
		return scheduleDao.save(schedule);
	}
	
	//Find all schedules
	public List<Schedule> findAll()
	{
		return scheduleDao.findAll();
	}
	
	//Find schedule by id
		public Schedule findSchedule(int id)
		{
			return scheduleDao.findByScheduleId(id);
		}
		
		// Update schedule
		public Schedule updateSchedule(Schedule schedule) {
				
				return scheduleDao.save(schedule);
		}
			
	//Delete schedule By id
		public int deleteSchedule(int id) {
		    if(scheduleDao.existsById(id))
			{
		    	scheduleDao.deleteById(id);
			  return 1;
			}else
				return 0;
		}

		public Schedule addSchedule1(ScheduleDto scheduleDto) {
			Schedule sc=new Schedule();
			sc.setDepartureDate(scheduleDto.getDepartureDate());
			sc.setArrivalDate(scheduleDto.getArrivalDate());
			sc.setDepartureTime(scheduleDto.getDepartureTime());
			sc.setArrivalTime(scheduleDto.getArrivalTime());
			sc.setFlightStatus("Scheduled");
			System.out.println(scheduleDto.getFlightId());
			System.out.println(flightDao.findByFlightId(scheduleDto.getFlightId()));
			sc.setFlight(flightDao.findByFlightId(scheduleDto.getFlightId()));
			
			return scheduleDao.save(sc);
		}
}
