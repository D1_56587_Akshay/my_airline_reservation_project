
package com.app.airline.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.Response;
import com.app.airline.pojos.PaymentCardDetails;
import com.app.airline.services.PaymentCardServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/paymentcards")
@RestController
public class PaymentCardControllerImpl {

	@Autowired
	private PaymentCardServiceImpl paymentCardService;

// Get Payment Card Details by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int paymentCardDetailsid) {
		try {
			PaymentCardDetails result = paymentCardService.findPaymentCardDetailsById(paymentCardDetailsid);
			if (result == null)
				return Response.error("PaymentCardDetails not found");
			System.out.println(result);
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	// Get Payment Card Details by user Id
		@GetMapping("/findByUserId/{id}")
		public ResponseEntity<?> findByUserId(@PathVariable("id") int userId) {
			try {
				List<PaymentCardDetails> result = paymentCardService.findPaymentCardDetailsByUserId(userId);
				if (result == null)
					return Response.error("PaymentCardDetails not found");
				System.out.println(result);
				return Response.success(result); // http status = 200, body = result
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}

// Get all Payment Card Details 
	@GetMapping("/getAllCards")
	public ResponseEntity<?> findAllPaymentCardDetails() {
		try {
			List<PaymentCardDetails> result = paymentCardService.findAllPaymentCardDetails();
//			if (result != null)
//				return result;
//			return null;
			if (result == null)
				return Response.error("PaymentCardDetails not found");
			System.out.println(result);
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
		
	}

	// Add Payment Card Details
	@PostMapping("/add")
	public ResponseEntity<?> addPaymentCardDetails(@Valid @RequestBody PaymentCardDetails paymentCardDetails) {
		try {
			PaymentCardDetails card = paymentCardService.addpaymentCardDetails(paymentCardDetails);
			if (card == null)
				return Response.error("Failed to add payment Card Details");
			return Response.success(card);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Update Payment Card Details by id
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePaymentCardDetailsById(@PathVariable("id") int id,
			@RequestBody PaymentCardDetails paymentCardDetails) {
		PaymentCardDetails card=paymentCardService.updatePaymentCardDetails(paymentCardDetails);
		if(card==null) {
			return Response.error("Payment Card not found by this ID");
		}
		else {
			return Response.success(card);
		}
	}

// Delete Payment Card Details by id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePaymentCardDetailsById(@PathVariable("id") int id) {
		try {
			int x = paymentCardService.deletePaymentCardDetails(id);
			if (x == 1)
				return Response.success(x + " PaymentCardDetails deleted");
			return Response.error("Payment Card not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
