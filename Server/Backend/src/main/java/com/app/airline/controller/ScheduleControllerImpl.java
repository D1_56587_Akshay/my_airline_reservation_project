package com.app.airline.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.Response;
import com.app.airline.dtos.ScheduleDto;
import com.app.airline.pojos.Schedule;
import com.app.airline.services.ScheduleServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/schedules")
@RestController
public class ScheduleControllerImpl {

	@Autowired
	private ScheduleServiceImpl scheduleService;
	
	//Add a schedule
	@PostMapping("/")
	public ResponseEntity<?> addSchedule(@RequestBody Schedule schedule)
	{
		try {
			Schedule s=scheduleService.addSchedule(schedule);
			if(s!=null)
			return Response.success(s);
			else
				return Response.error("Failed to add schedule");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
		
	}
	
	//Add a schedule
		@PostMapping("/add")
		public ResponseEntity<?> addSchedule1(@RequestBody ScheduleDto scheduleDto)
		{
			try {
				Schedule s=scheduleService.addSchedule1(scheduleDto);
				if(s!=null)
				return Response.success(s);
				else
					return Response.error("Failed to add schedule");
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
			
		}
	
	//Find all schedules
	@GetMapping("/")
	public ResponseEntity<?> findAll()
	{
		try {
			List<Schedule> list=scheduleService.findAll();
			if(list!=null)
				return Response.success(list);
			return Response.error("No schedules available currently");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//Get Schedule by ID
		@GetMapping("/{id}")
		public ResponseEntity<?> FindscheduleById( @PathVariable("id") int id) {
			try {
				Object result = scheduleService.findSchedule(id);
				if (result == null)
					return Response.error("schedule by this id does not exists");
				return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		
		// Update Schedule
			@PutMapping("/{id}")
			public ResponseEntity<?> updateSchedule(@PathVariable("id") int id, @RequestBody Schedule schedule){
				try {
					Object f=scheduleService.findSchedule(id);
					if(f==null)
						return Response.error("Schedule by this id does not exists");
					else
						{
						Schedule s=(Schedule)f;
						s.setFlightStatus(schedule.getFlightStatus());
						scheduleService.updateSchedule(s);
						return Response.success(f);
						}
				} catch (Exception e) {
					return Response.error(e.getMessage());
				}
			}
			
		// Delete Schedule by Id
			@DeleteMapping("/{id}")
			public ResponseEntity<?> deleteFlight(@PathVariable("id") int id) {
				try {
					Object f= scheduleService.findSchedule(id);
					if (f == null)
						return Response.error("Schedule by this id does not exists");
					else
					{
						Schedule s=(Schedule)f;
						int x=scheduleService.deleteSchedule(id);
						return Response.success(x+" Schedule deleted");
					}
					
				} catch (Exception e) {
					return Response.error(e.getMessage());
				}
			}
}
