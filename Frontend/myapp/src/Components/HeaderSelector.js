import AfterLoginHeader from "./AfterLoginHeader"
import Header from "./Header"
import AdminHeader from "./AdminHeader"

const HeaderSelector = () =>{
    const loginStatus = sessionStorage['loginStatus']
	const role = sessionStorage['role']
    console.log(role)
    if(loginStatus=='1' && role == 'user')
    return <AfterLoginHeader />
	else if(loginStatus=='1' && role == 'admin')
    return <AdminHeader/>
    else
    return <Header />
  }

  export default HeaderSelector