import { useLocation, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { useState , useEffect} from "react";
import HeaderSelector from "../Components/HeaderSelector";
import { toast } from "react-toastify";
import { URL } from "../config";
import axios from "axios";
import React from "react";
import Footer from "../Components/Footer";

const BookingDetails = () => {
  const navigate = useNavigate()
  // const { state } = useLocation();
  // const { selectedFlightFare, selectedFlightId, selectedFlightOtherFare } = state;
  const fare=sessionStorage.getItem('fare')
  const otherFare=sessionStorage.getItem('otherFare')
  const [title, setTitle]=useState("Mr")
  const [firstName, setFirstName]=useState(sessionStorage.getItem('firstName'))
  const [lastName, setLastName]=useState(sessionStorage.getItem('lastName'))
  const [email, setEmail]=useState(sessionStorage.getItem('email'))
  const [mobileNo, setMobileNo]=useState("")
  const [dateOfBirth, setDateOfBirth]=useState("")
  const [covidVaccinated, setCovidVaccinated]=useState(1)
  const [address, setAddress]=useState("")
  const [city, setCity]=useState("")
  const [passState, setPassState]=useState("")
  const [country, setCountry]=useState("")
  const [handBagWt, setHandBagWt]=useState("")
  const [checkInBagWt, setCheckInBagWt]=useState("")
  let totalBaggage= parseInt(handBagWt) + parseInt(checkInBagWt)
  console.log(fare);
  console.log(otherFare);
 // console.log(selectedFlightId);

  const SavePassenger = () => {
    
    if(mobileNo.length < 10  ){
      toast.warning("Enter valid mobile no")
    }else if(dateOfBirth.length==0){
      toast.warning("Enter date of birth")
    }else if(address.length==0){
      toast.warning("Enter address")
    }else if(city.length==0){
      toast.warning("Enter city")
    } else if(passState.length==0){
      toast.warning("Enter state")
    }else if(country.length==0){
      toast.warning("Enter country")
    }else if(handBagWt.length==0){
      toast.warning("Enter hand bag weight")
    }else if(checkInBagWt.length==0){
      toast.warning("Enter check in bag weight")
    }else {
      const body = {
        title,
        firstName,
        lastName,
        mobileNo,
        email,
        country,
        passState,
        city,
        address,
        dateOfBirth,
        covidVaccinated
      }

      const url = `${URL}/user/addPassenger`

      axios.put(url,body).then((response)=>{
        const result = response.data
        console.log(result['data'])
        if (result['status'] == 'success') {
          toast.success('Passenger details saved')

          // get the data sent by server
          const { id, firstName, lastName, email } = result['data']

          // persist the logged in user's information for future use
          sessionStorage['pId'] = id
          sessionStorage['pFirstName'] = firstName +' '+ lastName
          sessionStorage['totalBaggage'] = totalBaggage
          //sessionStorage['checkInBagWt'] = checkInBagWt
          

          // navigate to home component
          navigate('/user/selectPackage')
        } else {
          toast.error('error')
        }
      })
    }
  }



  return (
    <div style={{backgroundColor:'#E5E4E2'}} >
      <HeaderSelector />
      <div class="col mt-4 d-flex justify-content-center">
      <h1 className="title">Booking Details</h1>
      </div>
      <div className="row">
        <div  style={{width:'450px'}}>
          <div className="form fw-bold">
            <div style={{marginLeft:'340px',width:'100px', height:'545px',}}>
              <label htmlFor="" className="label-control">
                Title
              </label>
              <select
                class="form-select form-control"
                id="inputGroupSelect02"
                placeholder="title"
                onChange={(e) => {
                  setTitle(e.target.value);
                }}
              >
                <option value="Mr.">Mr</option>
                <option value="Mrs.">Mrs</option>
                <option value="Miss.">Miss</option>
              </select>
            </div>
          </div>
        </div>
        <div className="col" >
          <div className="form fw-bold" style={{marginLeft:'20px',width:'350px'}}>
            <div className="mb-3" >
              <label htmlFor="" className="label-control">
                First Name
              </label>
              <input value={firstName}
                onChange={(e) => {
                  setFirstName(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Email Id
              </label>
              <input
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="email"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Date of Birth
              </label>
              <input
                onChange={(e) => {
                  setDateOfBirth(e.target.value);
                }}
                type="date"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Address
              </label>
              <input
                onChange={(e) => {
                  setAddress(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                State
              </label>
              <input
                onChange={(e) => {
                  setPassState(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Hand Bag Weight
              </label>
              <input
                onChange={(e) => {
                  setHandBagWt(e.target.value);
                }}
                type="number"
                className="form-control"
              />
            </div>
            
            <div className="mb-3">
              <button onClick={()=>{navigate('/user/flightDetails')}} className="btn btn-primary" style={{backgroundColor:'#5C0632'}}>
                Back
              </button>
            </div>
          </div>
        </div>
        <div className="col" style={{marginRight:'250px'}}>
        <div className="form fw-bold" style={{width:'350px'}}>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Last Name
              </label>
              <input
              value={lastName}
                onChange={(e) => {
                  setLastName(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Mobile No
              </label>
              <input maxlength="10"
                onChange={(e) => {
                  setMobileNo(e.target.value);
                }}
                type="phone"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Covid Vaccinated Status
              </label>
              <select class="form-select form-control" id="inputGroupSelect02"
                                 onChange={(e) => {setCovidVaccinated(e.target.value)}} >
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                    </select>
              {/* <input
                onChange={(e) => {
                  setCovidVaccinated(e.target.value);
                }}
                type="text"
                className="form-control"
              /> */}
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                City
              </label>
              <input
                onChange={(e) => {
                  setCity(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Country
              </label>
              <input
                onChange={(e) => {
                  setCountry(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control" >
                Check In Bag Weight
              </label>
              <input
                onChange={(e) => {
                  setCheckInBagWt(e.target.value);
                }}
                type="number"
                className="form-control"
              />
            </div>
            
            <div className="mb-3">
              <button onClick={SavePassenger} className="btn btn-primary" style={{backgroundColor:'#5C0632'}}>
                Continue
              </button>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  );
};
export default BookingDetails;
