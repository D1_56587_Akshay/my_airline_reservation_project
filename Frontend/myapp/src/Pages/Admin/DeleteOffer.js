import axios from "axios"
import { useEffect } from "react"
import { useLocation, useNavigate } from "react-router"
import { toast } from "react-toastify"
import { URL } from "../../config"

const DeleteOffer = () =>{

    const { state } = useLocation()
    const { offerid } = state
    const navigate = useNavigate()
    console.log(offerid)

    useEffect( () =>{
                    DeleteSelectedOffer()
                    } ,[] )

function DeleteSelectedOffer(){

    const url=`${URL}/offers/${offerid}`
    axios.delete(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success'){
                toast.success('Offer deleted successfully') 
                navigate('/editoffer')       
        }else{
            toast.error(result['error'])
            
        }

    })
}

return (
    <div></div>
        )
}
export default DeleteOffer