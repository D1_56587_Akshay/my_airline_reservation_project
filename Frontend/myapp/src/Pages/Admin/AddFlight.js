import { useLocation, useNavigate } from "react-router";
import CurrencyInput from 'react-currency-input-field'
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import HeaderSelector from "../../Components/HeaderSelector";
import Footer from "../../Components/Footer";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const AddFlight = () => {
  const navigate = useNavigate()

  const[airlineName, setAirlineName] = useState('')
  const[departureAirportName ,setDepartureAirportName ] = useState('')
  const[arrivalAirportName , setarrivalAirportName ] = useState('')
  const[sourceCity, setSourceCity] = useState('')
  const[destinationCity,setDestinationCity ] = useState('')
  const[businessClassFare,setBusinessClassFare ] = useState(0.0)
  const[economyClassFare, setEconomyClassFare ] = useState(0.0)
  const[businessClassCapacity,setBusinessClassCapacity] = useState(0)
  const[economyClassCapacity,setEconomyClassCapacity] = useState(0)
  const[flightTotalCapacity, setFlightTotalCapacity] = useState(0)
 
  const AddNewFlight = () =>{
if(airlineName.length == 0){
toast.warning('Please enter Airline name')
}else if(departureAirportName.length == 0){
  toast.warning('Please enter Departure airport name')
}else if(arrivalAirportName.length == 0){
  toast.warning('Please enter Arrival airport name')
}else if(sourceCity.length == 0){
  toast.warning('Please enter Source city')
}else if(destinationCity.length == 0){
  toast.warning('Please enter Destination city')
}else if(businessClassFare == 0){
  toast.warning('Please enter Business class fare')
}else if(economyClassFare == 0){
  toast.warning('Please enter Economy class fare')
}else if(businessClassCapacity == 0){
  toast.warning('Please enter Business class capacity')
}else if(economyClassCapacity == 0){
  toast.warning('Please enter Economy class capacity')
}else if(flightTotalCapacity == 0){
  toast.warning('Please enter Flight total capacity')
}else {
const body = {
  airlineName,
  departureAirportName,
  arrivalAirportName,
  sourceCity,
  destinationCity,
  businessClassFare,
  economyClassFare,
  businessClassCapacity,
  economyClassCapacity,
  flightTotalCapacity,
}

      // url to call the api
      const url = `${URL}/admin/flight/add`

      // http method: post
      // body: contains the data to be sent to the API
      axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully added new flight')
          // navigate to the signin page
          navigate('/adminhome')
        } else {
          toast.error(result['error'])
        }
      })

}

  }

  return (
    <div style={{backgroundColor:'#E5E4E2', height:'100%'}}>
        <HeaderSelector/>
        <div className="col mt-4 d-flex justify-content-center"><h1>Add Flight Details</h1></div>

      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
        <div className="container p-3">
          <div style={styles.div}>
            <table>
                <tbody>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="airlinename">Airline name </label>
                </td>
                <td style={styles.td1}>
                  <label htmlFor="depname">Departure airport name </label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setAirlineName(e.target.value)
                }}
                type="text"
                className="form-control"
              />
                </td>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setDepartureAirportName(e.target.value)
                }}
                type="text"
                className="form-control"
              />

                </td>
              </tr>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="airlinename">Arrival airport name</label>
                </td>
                <td style={styles.td1}>
                  <label htmlFor="depname">Source city </label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setarrivalAirportName(e.target.value)
                }}
                type="text"
                className="form-control"
              />
                </td>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setSourceCity(e.target.value)
                }}
                type="text"
                className="form-control"
              />

                </td>
              </tr>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="airlinename">Destination city </label>
                </td>
                <td style={styles.td1}>
                  <label htmlFor="depname">Business class fare </label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setDestinationCity(e.target.value)
                }}
                type="text"
                className="form-control"
              />
                </td>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setBusinessClassFare(e.target.value)
                }}
                type="number"
                className="form-control"
              />

                </td>
              </tr>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="airlinename">Economy class fare </label>
                </td>
                <td style={styles.td1}>
                  <label htmlFor="depname">Business class capacity </label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setEconomyClassFare(e.target.value)
                }}
                type="number"
                className="form-control"
              />
                </td>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setBusinessClassCapacity(e.target.value)
                }}
                type="number"
                className="form-control"
              />

                </td>
              </tr>
              <tr style={{ color: "white" }}>
                <td style={styles.td1}>
                  <label htmlFor="airlinename">Economy class capacity </label>
                </td>
                <td style={styles.td1}>
                  <label htmlFor="depname">Flight total capacity </label>
                </td>
              </tr>
              <tr>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setEconomyClassCapacity(e.target.value)
                }}
                type="number"
                className="form-control"
              />
                </td>
                <td style={styles.td}>
                <input onChange={(e) => {
                  setFlightTotalCapacity(e.target.value)
                }}
                type="number"
                className="form-control"
              />

                </td>
              </tr>
            </tbody>
          </table>
          </div>
          <div className="row">

          <div className="col mt-4 d-flex justify-content-end">
            <button className="btn btn-primary custom-button px-5 justify-content-end" 
            style={{backgroundColor:'#5C0632'}}
            onClick={AddNewFlight}
            >
              Add Flight
            </button>
          </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  );
};

export default AddFlight