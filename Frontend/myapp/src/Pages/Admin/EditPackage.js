import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import HeaderSelector from "../../Components/HeaderSelector"
import Footer from "../../Components/Footer"

const EditPackage = () => {
    const [ packages, setPackage] = useState([])
    const navigate = useNavigate()
    
    useEffect(ViewPackages,[])

function ViewPackages(){

    const url=`${URL}/getAllPackages/`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setPackage(result['data'])
                toast.info('Exclusive packages just for you')       
        }else{
            toast.warning('No packages available now')
            navigate('/adminhome') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>
            <div>
          <br/>
            <h2 style={{textAlign:"center", color:"#5C0632"}}> Exclusive packages for you..........!!!</h2>
            <button className="btn btn-warning btn-lg float-end" onClick={()=>{navigate('/addpackage')}}>Click here to add new package</button>
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Package ID</th>
                        <th>Seat type</th>
                        <th>Food category</th>
                        <th>Beverages</th>
                        <th>Baggage allowance</th>
                        <th>Package fare</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        packages.map( (p) => (
                            <tr key={p.packageId}>
                                <td>{p.packageId}</td> 
                               <td>{p.seatType}</td> 
                               <td>{p.food}</td>
                                <td>{p.beverages}</td>
                                <td>{p.baggage}</td>
                                <td>{p.packageFare}</td>
                                <td>
                                    <button className="btn btn-danger"
                                   onClick={()=>{navigate('/deletepackage',{state:{packageid :p.packageId}})}} >Delete package</button>
                                </td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Footer/>
        </div>
    )
}

export default EditPackage