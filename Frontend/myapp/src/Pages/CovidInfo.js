
import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";
import HeaderSelector from "../Components/HeaderSelector";



const styles = {
    heading: {
        fontWeight: "bold",
        fontSize: '20px',
        padding: '40px',
        marginLeft: '15px'

    },

    content: {
        fontSize: '15px',
        marginLeft: '45px'
    }


}


const CovidInfo = () => {


    return (
        <div>
            <HeaderSelector />
            {/* <Content2/>
    <Content3 /> */}
            {/* <Content4/> */}

            <div style={{ backgroundColor: '#E5E4E2' }} >
                <br />
                <h1
                    style={{ textAlign: "center", fontWeight:'bolder' }}>
                    COVID-19 INFORMATION
                </h1>
                <div>
                    <div style={styles.heading}>
                    Whether you are planning to travel soon or have a booking that was affected by Covid-19, this page may have the additional information you need to help you book confidently and travel safely during this challenging time.
                    <br/> <br/>                  

                    While the information reflected here is updated regularly, entry requirements may change at short notice and customers are advised to check the latest travel advisory issued by the relevant local authorities before their trip.
                    
                    <br/><br/>
                    </div>      
                    
                    <div style={styles.content}>We seek your kind assistance to safeguard your well-being, as well as that of your fellow passengers and our crew on board our flights, by following the guidelines set out below:
                    <br/><br/>                    
                    1. All customers should wear a mask and sanitize their hands before proceeding to the boarding gate.
                    <br/><br/>
                    2. Customers must wear a face mask covering their nose and mouth, throughout their journey. The mask may be removed only while eating and drinking.
                    <br/><br/>
                    3. Please maintain appropriate social distancing while boarding and de-boarding the aircraft.
                    <br/><br/>
                    4. Kindly adhere to all the announcements and other directives issued by our ground staff and/or crew at all points of time during your journey.
                    <br/><br/>
                    5. Customers are also requested to familiarise themselves with the guidelines for air passengers published by the Indian Ministry of Civil Aviation.
                    <br/><br/>
                    </div>

                </div>
            </div>
            <Content4 />
            <Footer />
        </div>
    );
}

export default CovidInfo