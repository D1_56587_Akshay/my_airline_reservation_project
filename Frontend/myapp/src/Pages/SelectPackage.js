import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import Footer from "../Components/Footer";
import HeaderSelector from "../Components/HeaderSelector";
import { URL } from "../config";

const SelectPackage = () => {
    const navigate=useNavigate()
  const [selectedPackageId, setSelectedPackageId] = useState("");
  const [selectedPackageFare, setSelectedPackageFare] = useState("");
  const [selectedPackageName, setSelectedPackageName] = useState("");
  const [travelClass, setTravelClass]=useState(sessionStorage.getItem('travelClass'))//economy//business
  const [selectedFlightFare,setSelectedFlightFare]=useState(sessionStorage.getItem('fare'))//1000
  const [selectedFlightOtherFare,setSelectedFlightOtherFare]=useState(sessionStorage.getItem('otherFare'))//1500
  const [extraBaggageAllowed, setExtraBaggageAllowed]=useState("")
  const[result, setResult]=useState([])
  const[seatType, setSeatType]=useState("")//business//economy
    sessionStorage['selectedPackageId']=selectedPackageId
    sessionStorage['selectedPackageFare']=selectedPackageFare
    sessionStorage['travelClass']=travelClass//business//economy
    sessionStorage['fare']=selectedFlightFare//1500//1000
    sessionStorage['otherFare']=selectedFlightOtherFare//1000//1500
    sessionStorage['offerId']=10
    sessionStorage['offerDiscount']=100
    sessionStorage['extraBaggageAllowed']=extraBaggageAllowed
    sessionStorage['selectedPackageName']=selectedPackageName
  console.log(selectedPackageFare)
  console.log(selectedPackageId)
  useEffect(() => {
    const getPackageList = () => {
      const url = `${URL}/getAllPackages`;
      axios.get(url).then((response) => {
      const packagelist=response.data
        setResult(packagelist['data'])
        console.log(response)
      });
    };
    getPackageList();
  }, []);
// var result=JSON.parse(sessionStorage.getItem("packages"))
  console.log(result)
  
  const Continue=()=>{
    if(selectedPackageId.length==0){
      toast.warning("Please select package")
    }else{
      navigate('/user/paymentDetails',
      {state : {seatType: seatType, selectedPackageFare:selectedPackageFare, selectedPackageName:selectedPackageName}})
    }
  }

  return (
    <div>
      <HeaderSelector />
      <div class="col mt-4 d-flex justify-content-center">
      <h1>Package Details</h1>
      </div>
      <table className="table" >
        <thead>
          <tr>
            <th>Package Id</th>
            <th>Package Name</th>
            <th>Seat Type</th>
            <th>Food</th>
            <th>Beverages</th>
            <th>Baggage</th>
            <th>Pckage Fare</th>
          </tr>
        </thead>

        <tbody>
            {
                
          result.map(p => (
            <tr key={p.packageId}>
            <td>{p.packageId}</td>
            <td>{p.packageName}</td>
            <td>{p.seatType}</td>
            <td>{p.food}</td>
            <td>{p.beverage}</td>
            <td>{p.baggage}</td>
            <td>
              <input
                type="radio"
                id="package fare"
                name="packagefare"
                value={p.packageFare}
                onChange={(e) => {
                  setSelectedPackageFare(e.target.value);
                  setSelectedPackageName(p.packageName);
                  setSelectedPackageId(p.packageId);
                  setSeatType(p.seatType);
                  setExtraBaggageAllowed(p.baggage)
                  if(p.seatType!=travelClass){   
                        setTravelClass(p.seatType)
                        setSelectedFlightFare(sessionStorage.getItem('otherFare'))
                        setSelectedFlightOtherFare(sessionStorage.getItem('fare'))
                  }
                }}
              ></input>
              Rs. {p.packageFare}
            </td>
          </tr>
          ))
          
          }
          
        </tbody>
      </table>
      <div class='row'>
          <div class="col d-grid gap-2 d-md-flex" style={{marginLeft:'150px'}}>
          <button
            onClick={()=>{navigate('/user/bookingDetails')}}
            className="btn btn-primary" style={{backgroundColor:'#5C0632'}}>Back</button>
          </div>
      
            
            <div class=" col d-grid gap-2 d-md-flex justify-content-md-end" style={{marginRight:'150px'}}>
        <button
            onClick={Continue}
            className="btn btn-primary " style={{backgroundColor:'#5C0632'}}>Continue</button>
</div>
</div> 
{/* <div class=" col d-grid gap-2 d-md-flex justify-content-md-center" style={{marginRight:'150px'}}>
        <button
            onClick={refresh}
            className="btn btn-primary " style={{backgroundColor:'#5C0632'}}>Refresh</button>
</div> */}
<Footer/>
    </div>
  );
};

export default SelectPackage;
