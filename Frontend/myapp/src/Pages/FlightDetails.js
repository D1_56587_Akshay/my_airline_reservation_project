import { useLocation } from "react-router"
import { useNavigate } from "react-router"
import { useState } from "react"
import HeaderSelector from "../Components/HeaderSelector"
import Footer from "../Components/Footer"



const FlightDetails=()=>{
    const navigate = useNavigate()
    // const {state}=useLocation()
    // const { result } = state
    //const { result }=sessionStorage.getItem['searchFlightResult']
    const[travelClass, setTravelClass]=useState(sessionStorage.getItem('travelClass'))
    var result=JSON.parse(sessionStorage.getItem("searchFlightResult"))
    const[selectedFlightId, setSelectedFlightId]=useState()
    const[selectedFlightFare, setSelectedFlightFare]=useState()
    const[selectedFlightOtherFare, setSelectedFlightOtherFare]=useState()
    const[selectedScheduleId, setSelectedScheduleId]=useState("")
    console.log(result)
    sessionStorage['flightId']=selectedFlightId
    sessionStorage['fare']=selectedFlightFare
    sessionStorage['otherFare']=selectedFlightOtherFare
    sessionStorage['travelClass']=travelClass
    sessionStorage['scheduleId']=selectedScheduleId
    
    return(
        <div style={{width:'100%', height:'100%',backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>
        <div className="container" style={{width:'100%', height:'645px',backgroundColor:'#E5E4E2'}}>

        <h1>Flight List</h1>
        <div >

        <table className="table"  >
            <thead>

                <tr>
                   <th>Id</th>
                   <th>Airline Name</th>
                   <th>Departure Time</th>
                   <th>Duration</th>
                   <th>Arrival Time</th>
                   <th>Business Class</th> 
                   <th>Economy Class</th> 
                </tr>

            </thead>

            <tbody>
                {
                    result.map((flight)=>(
                        <tr key={flight.flightId}>
                            <td>{flight.flightId}</td>
                            <td>{flight.airlineName}</td>
                            <td>{flight.departureTime}</td>
                            <td>{flight.duration}</td>
                            <td>{flight.arrivalTime}</td>
                            <td>
                            <input type="radio" id="businessClass" name="classfare" value={flight.businessClassFare} 
                            onChange={(e)=>{setSelectedFlightFare(e.target.value);setSelectedFlightId(flight.flightId);
                            setTravelClass('business');setSelectedFlightOtherFare(flight.economyClassFare);setSelectedScheduleId(flight.scheduleId)}}></input>
                               Rs. {flight.businessClassFare}</td>
                            <td>
                            <input type="radio" id="arrivalClass" name="classfare" value={flight.economyClassFare} 
                            onChange={(e)=>{setSelectedFlightFare(e.target.value);setSelectedFlightId(flight.flightId);
                            setTravelClass('economy');setSelectedFlightOtherFare(flight.businessClassFare);setSelectedScheduleId(flight.scheduleId)}}></input>
                            Rs. {flight.economyClassFare}</td>
                            
                        </tr>
                    )
                    )
                }
            </tbody>


        </table>
        <button
            onClick={()=>{navigate('/')}}
            className="btn btn-primary">Back</button>
        <button
            onClick={()=>{navigate('/user/bookingDetails',{ state: { selectedFlightFare: selectedFlightFare, selectedFlightId: selectedFlightId, selectedFlightOtherFare: selectedFlightOtherFare} })}}
            className="btn btn-primary float-end">Continue</button>

        </div>

        </div>
        <Footer/>
        </div>
    )

}
export default FlightDetails